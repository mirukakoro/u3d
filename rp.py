import sys

from direct.showbase.ShowBase import ShowBase


class RPShowBase(ShowBase):
  def __init__(self):
    # raise RuntimeError('RP not supported')
    # Not adding super call, since it is handled by render_pipeline
    sys.path.insert(0, "./render_pipeline")
    from rpcore import RenderPipeline
    self.render_pipeline = RenderPipeline()
    self.render_pipeline.create(self)