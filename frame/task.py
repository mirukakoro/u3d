"""
μ3d Frame world.py
"""
from frame.object import Object


class Task(Object):
  """
  World is a Panda3D ShowBase wrapper.
  """
  def __init__(self, func, world, name: str):
    super().__init__()
    self.world = world
    self.name = name
    def wrapped_func(*args, **kwargs):
      return func(world, *args, **kwargs)
    self.func = wrapped_func

  def add(self, **kwargs):
    """
    Task.add is a wrapper of direct.task.Task.TaskManager.add.
    :param kwargs:
    """
    self.world.base.task_mgr.add(self.func, **kwargs)

  def later(self, delay_time, **kwargs):
    """
    Task.later is a wrapper of direct.task.Task.TaskManager.do_method_later.
    :param kwargs:
    """
    self.world.base.task_mgr.do_method_later(delay_time, self.func, **kwargs)

  def rm(self):
    """
    Task.rm is a wrapper of direct.task.Task.TaskManager.remove.
    """
    self.world.base.task_mgr.remove(self.name)

  def __call__(self, *args, **kwargs):
    return self.func(*args, **kwargs)
