"""
μ3d Frame __physics.py
"""
from panda3d.bullet import BulletWorld
from panda3d.core import ClockObject

from .world import World
from .task import Task
from .object import Object


class Physics(Object):
  def __init__(
    self,
    *args,
    **kwargs,
  ):
    super().__init__()
    self.world = BulletWorld(*args, **kwargs)
    self._world = None

  def apply(self, world: World):
    self._world = world
    @Task(world, '__physics-update')
    def physics_update(task):
      """
      physics_update is a task to update the __physics in a World.
      """
      # NOTE: switch line below to something that uses world (eg world.global_clock?) if possible
      world.doPhysics(ClockObject.getGlobalClock().getDt())
      return task.cont
    physics_update.add()
