"""
μ3d Frame actor.py
"""
import uuid

from direct.showbase import ShowBaseGlobal
from panda3d.bullet import BulletSphereShape, BulletPlaneShape, BulletBoxShape, BulletCylinderShape, BulletCapsuleShape, \
  BulletConeShape, BulletTriangleMesh, BulletTriangleMeshShape, BulletRigidBodyNode
from panda3d.core import Vec3

from .object import Object

from direct.actor.Actor import Actor as PActor


# class Actor(Object):
#   """
#   Actor represents a collection of models.
#   """
#   def __init__(
#     self,
#     *children
#   ):
#     self.models = []
#     self.anims = []
#     named_children = {}
#     for i, child in enumerate(children):
#       named_children[i] = child
#       if isinstance(child, Model):
#         self.models.append(child)
#       elif isinstance(child, Anim):
#         self.anims.append(child)
#     super().__init__(**children)
#     self.children_ = children
#     self.actor = None
#
#   def apply(self, world):
#     for child in self.children_:
#       if isinstance(child, Model):
#         self.models.append(child)
#         child.apply(world)
#       elif isinstance(child, Anim):
#         self.anims.append(child)
#         child.apply(world)
#     self.actor = PActor(
#       models = (m.model for m in self.models),
#       anims = (a.anim for a in self.anims),
#     )


class Model(Object):
  """
  Model represents a 3D model.
  """
  def __init__(
    self,
    config: dict,
    name: str = str(uuid.uuid4()),
  ):
    super().__init__()
    self.world = None
    self.model = None
    self.name = name
    self.node = None
    self.np = None
    self.config = config
    self.path = config['path']
    self.scale = config['scale']
    self.pos = config['pos']
    self.hpr = config['hpr']
    self.flatten = config['flatten']
    self.mass = config['__physics']['mass']
    self.shapes_cfg = config['__physics']['shape']
    self.type_ = config['__physics']['type']
    self.shapes = []

  def apply(self, world):
    def dict_without(a, *keys):
      b = a.copy()
      for key in keys:
        if key in b.keys():
          b.pop(key)
      return b
    self.world = world
    # __physics
    if self.type_ == 'rigid':
      self.node = BulletRigidBodyNode(f'{self.name}-node')
    else:
      raise TypeError('more __physics types (ie softbody, etc) coming soon')
    # mass
    self.node.set_mass(self.mass)
    self.np = world.base.render.attachNewNode(self.node)
    self.world.__physics.world.attachRigidBody(self.node)
    # pos, hpr, etc
    self.np.set_pos(*self.pos)
    self.np.set_hpr(*self.hpr)
    self.np.set_scale(*self.scale)
    # model
    self.model = world.base.loader.load_model(self.path)
    # self.flatten
    # False - no flattening
    # light
    # medium
    # strong
    if self.flatten == "light":
      self.model.flatten_light()
    elif self.flatten == "medium":
      self.model.flatten_medium()
    elif self.flatten == "strong":
      self.model.flatten_strong()
    # self.model.reparentTo(self.np)
    self.model.reparentTo(self.world.base.render)
    # __physics shape
    for shape_cfg in self.shapes_cfg:
      type_ = shape_cfg['type']
      args = shape_cfg.get('args', [])
      kwargs = dict_without(shape_cfg, 'type', 'args')
      if type_ == 'sphere':
        self.node.add_shape(BulletSphereShape(*args, **kwargs))
      elif type_ == 'plane':
        self.node.add_shape(BulletPlaneShape(normal=Vec3(*shape_cfg['normal']), constant=shape_cfg['constant']))
      elif type_ == 'box':
        self.node.add_shape(BulletBoxShape(Vec3(*shape_cfg['half_extents'])))
      elif type_ == 'cylinder':
        self.node.add_shape(BulletCylinderShape(*args, **kwargs))
      elif type_ == 'capsule':
        self.node.add_shape(BulletCapsuleShape(*args, **kwargs))
      elif type_ == 'cone':
        self.node.add_shape(BulletConeShape(*args, **kwargs))
      elif type_ == 'auto':
        mesh = BulletTriangleMesh()
        mesh.add_geom(self.model.findAllMatches('**/+GeomNode').getPath(0).node().getGeom(0))
        shape = BulletTriangleMeshShape(mesh, *args, **kwargs)
        self.node.add_shape(shape)


# class Anim(Object):
#   """
#   Anim represents animations on an Actor.
#   """
#   def __init__(
#     self,
#   ):
#     super().__init__()
#     raise RuntimeError('Anim support coming soon')
