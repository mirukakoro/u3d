import jsonpath_ng
from direct.showbase.ShowBase import ShowBase

from .physics import Physics
from .config import Config


class Finder:
  def __init__(self):
    self.keys = []
  
  def __getitem__(self, item):
    self.keys.append(item)
    return self


class AppAt(dict):
  def __init__(self, app):
    self.app = app
  
  def __call__(self, event_name: str):
    pass
  
  def __getitem__(self, item: str):
    if not isinstance(item, str):
      raise TypeError(f'item must be str, not {type(item)}')
    return self.app.__data_get__(item)
  
  def __setitem__(self, key: str, value: object):
    if not isinstance(key, str):
      raise TypeError(f'key must be str, not {type(key)}')
    return self.app.__data_set__(key, value)


class AppTask:
  def __init__(self, app):
    self.app = app
  
  def __call__(self, event_name: str):
    pass
  
  def __getitem__(self, item: str):
    if not isinstance(item, str):
      raise TypeError(f'item must be str, not {type(item)}')
    return self.app.__data_get__(item)
  
  def __setitem__(self, key: str, value: object):
    if not isinstance(key, str):
      raise TypeError(f'key must be str, not {type(key)}')
    return self.app.__data_set__(key, value)


class App:
  def __init__(self, name: str, config: Config = None, physics: Physics = None):
    if not isinstance(name, str):
      raise TypeError(f'name must be str, not {type(name)}')
    self.name = name
    if not isinstance(config, Config) and config is not None:
      raise TypeError(f'config must be Config, not {type(config)}')
    if not isinstance(physics, Physics) and physics is not None:
      raise TypeError(f'physics must be Physics, not {type(physics)}')
    self.__data = {
      'config': config,
      'sys': {
        'physics': physics,
      },
      'tasks': {},
      'events': {},
    }
    self.at = AppAt(self)
    self.task = AppTask(self)
  
  @property
  def config(self):
    return self['$.sys.config']

  @config.setter
  def config(self, value: Config):
    self['$.sys.config'] = value
  
  @property
  def physics(self):
    return self['$.sys.physics']

  @physics.setter
  def physics(self, value: Physics):
    self['$.sys.physics'] = value
  
  def getdata(self):
    return self.__data
  
  def __getitem__(self, item: str):
    if not isinstance(item, str):
      raise TypeError(f'item must be str, not {type(item)}')
    return self.__data_get__(item)
  
  def __setitem__(self, key: str, value: object):
    if not isinstance(key, str):
      raise TypeError(f'key must be str, not {type(key)}')
    return self.__data_set__(key, value)
  
  def __call__(self):
    self.apply()
  
  def apply(self):
    base = ShowBase()
    self['$.sys.showbase'] = base
    if self['$.sys.physics'] is not None:
      self['$.sys.physics'].apply(self)
    base.run()
  
  def __data_get__(self, item: str):
    matches = jsonpath_ng.parse(item).find(self.__data)
    if len(matches) == 1:
      return matches[0].value
    elif len(matches) == 0:
      raise KeyError(item)
    else:
      raise KeyError(f'ambiguous query {item}')
  
  def __data_set__(self, key: str, value: object):
    finder = Finder()
    matches = jsonpath_ng.parse(key).find(finder)
    
    def set_(data: dict, key_: list, value: str):
      if len(key_) == 1:
        data[key_[0]] = value
      elif len(key_) == 0:
        raise TypeError(f'len of key_ must be greater than 0, but was 0')
      else:
        if key_[0] not in data.keys():
          data[key_[0]] = {}
        set_(data[key_[0]], key_[1:], value)
    
    if len(matches) == 1:
      set_(self.__data, finder.keys, value)
    elif len(matches) == 0:
      raise KeyError(key)
    else:
      raise KeyError(f'ambiguous query {key}')
